package jsondemo;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ObjectToJSon {

	public static void main(String[] args) {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		EmpDetails ed = createmyObject();
		String j = gson.toJson(ed);//java object to string
		System.out.println(j);
		
		//dont use throws... use try catch
		//java object  to file
		try (FileWriter writer = new FileWriter("C:\\Users\\bigafiq\\Desktop\\New folder\\myfile.json"))
		{
		gson.toJson(ed, writer);//to convert to json
		System.out.println("ok its done go and check your myfile.json");
		
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		}
	private static EmpDetails createmyObject() {
		EmpDetails ed = new EmpDetails();
		ed.setName("Ricky Ponting");
		ed.setAge(40);
		ed.setCity("Brisbane");
		return ed;
	}

}
