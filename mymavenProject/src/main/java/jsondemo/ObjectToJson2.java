package jsondemo;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ObjectToJson2 {

	public static void main(String[] args){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		StaffDetails sd = createObject();
		String s = gson.toJson(sd);//java object to string
		System.out.println(s);
		
		//dont use throws...use try and catch
		//java object  to file
				try(FileWriter writer = new FileWriter("C:\\Users\\bigafiq\\Desktop\\New folder\\myfile.json")){
				gson.toJson(sd, writer);
				System.out.println("ok its done go and check your myfile.json");
				}catch(IOException e) {
					e.printStackTrace();
				}
	}
	
	private static StaffDetails createObject() {
		StaffDetails sd = new StaffDetails();
		sd.setFname("Ricky");
		sd.setLname("Brisbane");
		return sd;
	}

}
