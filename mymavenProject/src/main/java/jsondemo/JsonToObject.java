package jsondemo;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

public class JsonToObject {

	public static void main(String[] args) {
		
		Gson gson = new Gson();
		
		try (Reader read = new FileReader("C:\\Users\\bigafiq\\Desktop\\New folder\\myfile.json")) {
		
			EmpDetails ed = gson.fromJson(read, EmpDetails.class);//convert to java object
			//EmpDetails ni dri class EmpDetails.java
			System.out.println(ed);//if dont have toString method in EmpDetails class... this output will display the memory location instead of the actual value
		
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		

	}

}
