package jsonpractice;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ObjToJson {

	public static void main(String[] args) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Details d = createObj();
		String s = gson.toJson(d);
		System.out.println(s);
		
		try(FileWriter wr = new FileWriter("C:\\Users\\bigafiq\\Desktop\\New folder\\practice.json")){
			gson.toJson(d, wr);
			System.out.println("Successfully write to practice.json");
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		

	}
	
	private static Details createObj() {
		Details d = new Details();
		d.setName("AFIQ");
		d.setAge(25);
		d.setGender('M');
		d.setCity("Kuala Krai");
		d.setCountry("Malaysia");
		return d;
	}

}
