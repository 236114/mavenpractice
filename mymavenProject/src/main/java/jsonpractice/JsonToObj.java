package jsonpractice;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

public class JsonToObj {

	public static void main(String[] args) {


		Gson gson = new Gson();
		
		try(Reader r = new FileReader("C:\\Users\\bigafiq\\Desktop\\New folder\\practice.json")){
			Details d = gson.fromJson(r, Details.class);
			System.out.println(d);
		}catch(IOException e) {
			e.printStackTrace();
		}

	}

}
